//
//  ViewController.swift
//  pantallas
//
//  Created by MARIANA CUADRADO on 14/7/17.
//  Copyright © 2017 MARIANA CUADRADO. All rights reserved.
//

import UIKit


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {


    @IBOutlet weak var myTableView: UITableView!
    
    let lista = ["Jhon", "Smith","180cm","United States","jhon.smith@hotmail.com"]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        cell.textLabel?.text = lista[indexPath.row]
        
        return (cell)
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (lista.count)
    }
}

