//
//  ProfileViewController.swift
//  pantallas
//
//  Created by MARIANA CUADRADO on 15/7/17.
//  Copyright © 2017 MARIANA CUADRADO. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var profileTableView: UITableView!
    
     let lista = ["Jhon", "Smith","180cm","United States","jhon.smith@hotmail.com"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (lista.count)
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "celda")
        cell.textLabel?.text = lista[indexPath.row]
        
        return (cell)
    }
    
    
    @IBAction func cancelarItemBar(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    
    }

}
